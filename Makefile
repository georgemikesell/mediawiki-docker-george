SHELL := /bin/bash

.DEFAULT: freshInstallWithExtensions
.PHONY: freshInstallWithExtensions
freshInstallWithExtensions:
	-@if [ ! -f "./mediawiki-docker-make/Makefile" ]; then \
		git submodule update --init; \
	fi
#	The include file should now be in place, so subsequent "make" calls will have access to the include vars
	make continueInstallation;

appendFileContentsToLocalSettings:
	cat ./extraLocalSettings.php >> "./mediawiki-docker-make/mediawiki/LocalSettings.php";

investigate_url = "http://localhost:$(mediawiki_port)/w/index.php?title=Special:UserLogin&returnto=Special:Investigate"

.PHONY: continueInstallation
continueInstallation:
	make freshinstall skipopenspecialversionpage=true;
	make appendFileContentsToLocalSettings;
	make useminervaneueskin skipopenspecialversionpage=true;
	make addIPInfoData;
	make installExtensions;
	make executeConfigurations;
	@echo -e "\nOpening '$(investigate_url)'.\nLog in using 'Admin' and 'dockerpass' if needed.\n";
	$(makefile_dir)/utility.sh open_url_when_available $(investigate_url);

.PHONY: addIPInfoData
addIPInfoData:
	cp ./GeoIP2-Anonymous-IP.mmdb "./mediawiki-docker-make/mediawiki/GeoIP2-Anonymous-IP.mmdb";
	cp ./GeoIP2-Enterprise.mmdb "./mediawiki-docker-make/mediawiki/GeoIP2-Enterprise.mmdb";
	cp ./GeoLite2-ASN.mmdb "./mediawiki-docker-make/mediawiki/GeoLite2-ASN.mmdb";
	cp ./GeoLite2-City.mmdb "./mediawiki-docker-make/mediawiki/GeoLite2-City.mmdb";
	cp ./GeoLite2-Country.mmdb "./mediawiki-docker-make/mediawiki/GeoLite2-Country.mmdb";

.PHONY: executeConfigurations
executeConfigurations:
	cp ./extraConfiguration.sh "./mediawiki-docker-make/mediawiki/extraConfiguration.sh";
	cd $(mediawiki_dir) && docker compose exec mediawiki /bin/bash -cl "/var/www/html/w/extraConfiguration.sh"

.PHONY: installExtensions
installExtensions:
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/IPInfo extensionBranch=master extensionSubdirectory=IPInfo wfLoadExtension=IPInfo;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/CheckUser extensionBranch=master extensionSubdirectory=CheckUser wfLoadExtension=CheckUser;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/EventLogging extensionBranch=master extensionSubdirectory=EventLogging wfLoadExtension=EventLogging;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/EventBus extensionBranch=master extensionSubdirectory=EventBus wfLoadExtension=EventBus;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/EventStreamConfig extensionBranch=master extensionSubdirectory=EventStreamConfig wfLoadExtension=EventStreamConfig;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/GlobalBlocking extensionBranch=master extensionSubdirectory=GlobalBlocking wfLoadExtension=GlobalBlocking;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/Phonos extensionBranch=master extensionSubdirectory=Phonos wfLoadExtension=Phonos;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/SecurityApi extensionBranch=master extensionSubdirectory=SecurityApi wfLoadExtension=SecurityApi;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/UploadWizard extensionBranch=master extensionSubdirectory=UploadWizard wfLoadExtension=UploadWizard;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/UrlShortener extensionBranch=master extensionSubdirectory=UrlShortener wfLoadExtension=UrlShortener;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/WikimediaEvents extensionBranch=master extensionSubdirectory=WikimediaEvents wfLoadExtension=WikimediaEvents;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/WikimediaMessages extensionBranch=master extensionSubdirectory=WikimediaMessages wfLoadExtension=WikimediaMessages;

-include ./mediawiki-docker-make/Makefile