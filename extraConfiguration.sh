#!/bin/bash

# sqlite3
apt update
apt install sqlite3

# GlobalBlocking - https://www.mediawiki.org/wiki/MediaWiki-Docker/Extension/GlobalBlocking
sqlite3 cache/sqlite/globalblocking.sqlite < extensions/GlobalBlocking/sql/sqlite/tables-generated-globalblocks.sql

# common
composer update
php maintenance/update.php

cd /var/www/html/w/extensions/CheckUser
composer install --no-dev

cd /var/www/html/w/extensions/IPInfo
composer install --no-dev

"$@"
